clean:
	find . -name __pycache__ | xargs rm -rf
	find . -name .pytest_cache | xargs rm -rf
	find . -name .mypy_cache | xargs rm -rf
	find . -name *pyc | xargs rm -rf

