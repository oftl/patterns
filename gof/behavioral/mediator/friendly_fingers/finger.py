import friendly_fingers.hand as hand

class Finger:

    def __init__(self, name: str = '', hand: hand.Hand = None):
        self.name: str = name
        self.hand: hand.Hand = hand

    def shake(self):
        print(f'{self.name} shaking')
        self.hand.keep_shaking(last=self.name)

    def raise_middle_finger(self):
        if self.name == 'middle_finger':
            print('middle_finger raised itself')
        else:
            print('make hand raise middle_finger')
            self.hand.raise_middle_finger()
