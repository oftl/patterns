import friendly_fingers.finger as finger
import friendly_fingers.hand as hand

# the mediator
hand = hand.Hand()

# the colleagues
thumb = finger.Finger(name='thumb', hand=hand)
hand.add_finger(thumb)

index_finger = finger.Finger(name='index_finger', hand=hand)
hand.add_finger(index_finger)

middle_finger = finger.Finger(name='middle_finger', hand=hand)
hand.add_finger(middle_finger)

ring_finger = finger.Finger(name='ring_finger', hand=hand)
hand.add_finger(ring_finger)

pinky = finger.Finger(name='pinky', hand=hand)
hand.add_finger(pinky)

# and ... action!
index_finger.shake()

# now the middlefinger
middle_finger.raise_middle_finger()
thumb.raise_middle_finger()
