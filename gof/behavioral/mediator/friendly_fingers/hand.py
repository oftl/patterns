import random

import friendly_fingers.finger as finger

class Hand:
    def __init__(self):
        self.fingers: list = []
        self.shaked: list = []

    # def add_finger(self, finger: finger.Finger = None):
    def add_finger(self, finger):
        self.fingers.append(finger)

    def keep_shaking(self, last: str = ''):
        self.shaked.append(last)

        if len(self.shaked) < len(self.fingers):
            remaining = [f for f in self.fingers if f.name not in self.shaked]
            candidates = random.sample(remaining, k=len(remaining))
            candidates.pop().shake()
        else:
            self.shaked = []
            print('done.')

    def raise_middle_finger(self):
        middle_finger = list([f for f in self.fingers if f.name is 'middle_finger']).pop()
        middle_finger.raise_middle_finger()
