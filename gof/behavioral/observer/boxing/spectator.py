from typing import List

from .observer import Observer
from .subject import Subject

class Spectator(Observer):

    def __init__(self, name: str, subjects: List[Subject] = []) -> None:
        self.name = name
        self.subjects: list = subjects

        for s in self.subjects:
            s.attach(self)

    def update(self, subject: Subject) -> None:
        print(f'[S] wow, did you see {subject.name}\'s puch ?')

    def done(self) -> None:
        for s in self.subjects:
            s.detach(self)
