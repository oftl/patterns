from typing import List

from .observer import Observer
from .subject import Subject

class Referee(Observer):

    def __init__(self, name: str, subjects: List[Subject]) -> None:
        self.name = name
        self.subjects: List[Subject] = subjects

        for s in self.subjects:
            s.attach(self)

    def update(self, subject: Subject) -> None:
        print(f'[R] {subject.name} punched hard')

    def done(self) -> None:
        for s in self.subjects:
            s.detach(self)
