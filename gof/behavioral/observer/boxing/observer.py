import abc

class Observer:

    @abc.abstractmethod
    def update(self, subject):
        pass

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value
