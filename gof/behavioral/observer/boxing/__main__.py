from .boxer import Boxer
from .referee import Referee
from .spectator import Spectator

if __name__ == '__main__':
    # subjects
    rocky: Boxer = Boxer(name='Rocky')
    muhammad: Boxer = Boxer(name='Muhammad')

    # observers
    referee: Referee = Referee(name='Referee', subjects=[rocky, muhammad])
    spectator01: Spectator = Spectator(name='Peter', subjects=[rocky, muhammad])
    spectator02: Spectator = Spectator(name='Paul', subjects=[rocky, muhammad])
    spectator03: Spectator = Spectator(name='Mary', subjects=[rocky, muhammad])

    for round in range(1, 4):
        print(f'[M] -- ROUND {round} --')

        rocky.punch()
        muhammad.punch()

    # when observers stop detach, obviousl they won't get notified anymore
    # all that remains is a couple of aging boxers that no one notices :-(
    #
    spectator01.done()
    spectator02.done()
    spectator03.done()

    for round in range(1, 4):
        print(f'[M] -- ROUND {round} --')

        rocky.punch()
        muhammad.punch()
