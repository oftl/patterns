from typing import List

from .subject import Subject
from .observer import Observer

class Boxer(Subject):

    def __init__(self, name):
        self.name = name
        self.observers: List[Observer] = []

    def attach(self, observer: Observer):
        self.observers.append(observer)

    def detach(self, observer: Observer):
        for i, o in enumerate(self.observers):
            if o.name == observer.name:
                del(self.observers[i])

    # def get_state(self):
    #     pass

    def notify(self):
        for o in self.observers:
            o.update(self)

    def punch(self):
        print(f'[B] {self.name} punching')
        self.notify()
