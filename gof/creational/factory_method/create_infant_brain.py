from create_brain import CreateBrain
from infant_brain import InfantBrain

class CreateInfantBrain(CreateBrain):

    def make_brain(self):
        return InfantBrain()
