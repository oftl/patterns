from abc import ABC, abstractmethod

class CreateBrain(ABC):

    @abstractmethod
    def make_brain(self):
        # NOTE CreateBrain.make_brain is callable through super()
        raise NotImplementedError
