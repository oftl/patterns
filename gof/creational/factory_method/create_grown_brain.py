from create_brain import CreateBrain
from grown_brain import GrownBrain

class CreateGrownBrain(CreateBrain):

    def make_brain(self):
        return GrownBrain()
