import pytest

from create_infant_brain import CreateInfantBrain
from create_grown_brain import CreateGrownBrain
from create_brain import CreateBrain

print('create an infant brain')
creator = CreateInfantBrain()
brain = creator.make_brain()
print(f'brain\'s kind is: {brain.kind}')

print('create a grown brain')
creator = CreateGrownBrain()
brain = creator.make_brain()
print(f'brain\'s kind is: {brain.kind}')

with pytest.raises(TypeError):
    print('create a - brain')
    creator = CreateBrain()
    brain = creator.make_brain()
    print(f'brain\'s kind is: {brain.kind}')
