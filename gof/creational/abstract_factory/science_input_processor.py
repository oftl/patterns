from input_processor import InputProcessor
from logger import Logger
import message_kinds
import re

log = Logger().logger()

class ScienceInputProcessor(InputProcessor):

    def input(self, msg: str) -> str:
        log.debug('[ScienceInputProcessor.input] msg: <{}>'.format(msg))

        if re.match('.*gravity.*', msg):
            msg_kind = message_kinds.GRAVITY
        else:
            msg_kind = message_kinds.UNKNOWN

        log.debug('[ScienceInputProcessor.input] message kind: <{}>'.format(msg_kind))

        return msg_kind
