import abc

# AbstractProduct
class InputProcessor(abc.ABC):

    @abc.abstractmethod
    def input(self, msg: str) -> str:
        pass
