import abc
from input_processor import InputProcessor
from output_shaper import OutputShaper

# AbstractFactory (WidgetFactory)
class RobotFactory(abc.ABC):
    def __init__(self):
        log.debug('[RobotFactory.__init__]')

    @abc.abstractmethod
    def createInputProcessor(self) -> InputProcessor:
        pass

    @abc.abstractmethod
    def createOutputShaper(self) -> OutputShaper:
        pass
