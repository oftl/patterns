from input_processor import InputProcessor
from logger import Logger
import message_kinds
import re

log = Logger().logger()

class SmalltalkInputProcessor(InputProcessor):

    def input(self, msg: str) -> str:
        log.debug('[SmalltalkInputProcessor.input] msg: <{}>'.format(msg))

        if re.match('.*Hello.*', msg):
            msg_kind = message_kinds.GREETING
        else:
            msg_kind = message_kinds.UNKNOWN

        log.debug('[SmalltalkInputProcessor.input] message kind: <{}>'.format(msg_kind))

        return msg_kind
