from robot_factory import RobotFactory
from science_input_processor import ScienceInputProcessor
from science_outputshaper import ScienceOutputShaper
from logger import Logger

log = Logger().logger()

class ScienceRobotFactory(RobotFactory):
    def __init__(self):
        log.debug('[ScienceRobotFactory.__init__]')

    def createInputProcessor(self) -> ScienceInputProcessor:
        return ScienceInputProcessor()

    def createOutputShaper(self) -> ScienceOutputShaper:
        return ScienceOutputShaper()
