import abc

# AbstractProduct
class OutputShaper(abc.ABC):

    @abc.abstractmethod
    def output(self, msg_type: str) -> str:
        pass
