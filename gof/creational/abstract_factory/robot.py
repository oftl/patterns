from input_processor import InputProcessor
from output_shaper import OutputShaper
from logger import Logger
from robot_factory import RobotFactory

log = Logger().logger()

class Robot:
    def __init__(self) -> None:
        log.debug('[Robot.__init__]')

    def input(self, msg: str) -> None:
        self._msg_type = self.input_processor.input(msg)

    def output(self):
        return self.output_shaper.output(self._msg_type)

    ### properties

    @property
    def input_processor(self) -> InputProcessor:
        return self._input_processor

    @input_processor.setter
    def input_processor(self, value: InputProcessor) -> None:
        self._input_processor = value

    @property
    def output_shaper(self) -> OutputShaper:
        return self._output_shaper

    @output_shaper.setter
    def output_shaper(self, value: OutputShaper) -> None:
        self._output_shaper = value

# NOTE
# GOF put this function in MazeGame
def create_robot (factory: RobotFactory) -> Robot:
    robot = Robot()
    input_processor = factory.createInputProcessor()
    output_shaper = factory.createOutputShaper()

    robot.input_processor = input_processor
    robot.output_shaper = output_shaper

    return robot
