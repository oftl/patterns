import logging
import sys

class Logger (object):
    """ logging mixin
    """

    _log_level = logging.DEBUG
    _logger = None

    @classmethod
    def logger (self):
        if not self._logger:
            handler = logging.StreamHandler()
            handler.setFormatter (logging.Formatter (fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'))

            logger = logging.getLogger('robot')
            logger.setLevel(self._log_level)
            logger.addHandler(handler)

            self._logger = logger

        return self._logger
