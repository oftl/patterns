from robot_factory import RobotFactory
from smalltalk_input_processor import SmalltalkInputProcessor
from smalltalk_outputshaper import SmalltalkOutputShaper
from logger import Logger

log = Logger().logger()

# ConcreteFactory (MotifWidgetFactory, PMWidgetFactory)
class SmalltalkRobotFactory(RobotFactory):
    def __init__(self):
        log.debug('[SmalltalkRobotFactory.__init__]')

    def createInputProcessor(self) -> SmalltalkInputProcessor:
        return SmalltalkInputProcessor()

    def createOutputShaper(self) -> SmalltalkOutputShaper:
        return SmalltalkOutputShaper()
