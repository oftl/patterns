from input_processor import InputProcessor
from output_shaper import OutputShaper
from logger import Logger
import message_kinds

log = Logger().logger()

class ScienceOutputShaper(OutputShaper):

    def output(self, msg_kind: str) -> str:
        log.debug('[ScienceOutputShaper.output] msg_kind: <{}>'.format(msg_kind))

        if msg_kind == message_kinds.GRAVITY:
            response = 'Fucking love it!'
        else:
            response = 'Do i look like a Bender to you?'

        log.debug('[ScienceOutputShaper.output] response: <{}>'.format(response))

        return response
