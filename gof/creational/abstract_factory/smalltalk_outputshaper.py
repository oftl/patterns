from input_processor import InputProcessor
from output_shaper import OutputShaper
from logger import Logger
import message_kinds

log = Logger().logger()

# ConcreteProduct
class SmalltalkOutputShaper(OutputShaper):

    def output(self, msg_kind: str) -> str:
        log.debug('[SmalltalkOutputShaper.output] msg_kind: <{}>'.format(msg_kind))

        if msg_kind == message_kinds.GREETING:
            response = 'Hello to you too!'
        else:
            response = 'What?'

        log.debug('[SmalltalkOutputShaper.output] response: <{}>'.format(response))

        return response
