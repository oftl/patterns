from smalltalk_robot_factory import SmalltalkRobotFactory
from science_robot_factory import ScienceRobotFactory
from robot_factory import RobotFactory
from robot import Robot, create_robot
from logger import Logger

log = Logger().logger()

def smalltalk():
    log.debug('[client.smalltalk]')

    st_factory: RobotFactory = SmalltalkRobotFactory()
    st_robot: Robot = create_robot(st_factory)

    st_robot.input ('Hello robot')
    assert st_robot.output() == 'Hello to you too!'

    st_robot.input ('Fancy a dring')
    assert st_robot.output() == 'What?'


def science():
    log.debug('[client.science]')

    science_factory: RobotFactory = ScienceRobotFactory()
    science_robot: Robot = create_robot(science_factory)

    science_robot.input ('Do you like gravity?')
    assert science_robot.output() == 'Fucking love it!'

    science_robot.input ('Fancy a dring')
    assert science_robot.output() == 'Do i look like a Bender to you?'

# Client
if __name__ == '__main__':
    log.debug('[client.__main__]')

    smalltalk()
    science()
