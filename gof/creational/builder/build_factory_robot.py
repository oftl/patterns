from director import Director
from factory_robot_builder import FactoryRobotBuilder

factory_robot_builder = FactoryRobotBuilder()

factory_director = Director(factory_robot_builder)
factory_director.build()

robot = factory_robot_builder.get_robot()

print (robot)
