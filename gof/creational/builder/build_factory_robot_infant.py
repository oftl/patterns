from director_infant import DirectorInfant
from factory_robot_builder import FactoryRobotBuilder

factory_robot_builder = FactoryRobotBuilder()

factory_director_infant = DirectorInfant(factory_robot_builder)
factory_director_infant.build()

robot = factory_robot_builder.get_robot()

print (robot)
