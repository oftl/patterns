from robot_builder import RobotBuilder
from robot import Robot

class FactoryRobotBuilder(RobotBuilder):

    def __init__(self) -> None:
        self._robot: Robot = Robot()

    def add_brain(self):
        self._robot.brain = 'Factory Brain'

    def add_knowledgebase(self):
        self._robot.knowledgebase = 'Factory Knowledgebase'

    def add_body(self):
        self._robot.body = 'Factory Body'

    def get_robot(self) -> Robot:
        return self._robot
