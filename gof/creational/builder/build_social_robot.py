from director import Director
from social_robot_builder import SocialRobotBuilder

social_rb = SocialRobotBuilder()

social_d = Director(social_rb)
social_d.build()

robot = social_rb.get_robot()

print (robot)
