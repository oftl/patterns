from robot_builder import RobotBuilder

class DirectorInfant:

    def __init__(self, builder: RobotBuilder) -> None:
        self._builder = builder

    def build(self) -> None:
        self._builder.add_brain()
        self._builder.add_body()
