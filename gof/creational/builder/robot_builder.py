import abc
from robot import Robot

class RobotBuilder(abc.ABC):

    @abc.abstractmethod
    def add_brain(self) -> None:
        pass

    @abc.abstractmethod
    def add_knowledgebase(self) -> None:
        pass

    @abc.abstractmethod
    def add_body(self) -> None:
        pass

    @abc.abstractmethod
    def get_robot(self) -> Robot:
        pass
