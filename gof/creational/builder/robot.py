class Robot():
    def __init__(self) -> None:
        self._brain: str = 'EMPTY'
        self._knowledgebase: str = 'EMPTY'
        self._body: str = 'EMPTY'

    def __str__(self):
        return(f'brain <{self._brain}>, knowledgebase <{self._knowledgebase}>, body <{self._body}>')

    @property
    def brain(self) -> str:
        return self._brain

    @brain.setter
    def brain(self, value: str) -> None:
        self._brain = value

    @property
    def knowledgebase(self) -> str:
        return self._knowledgebase

    @knowledgebase.setter
    def knowledgebase(self, value: str) -> None:
        self._knowledgebase = value

    @property
    def body(self) -> str:
        return self._body

    @body.setter
    def body(self, value: str) -> None:
        self._body = value
